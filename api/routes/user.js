const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const User = require('../models/user')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken');
const UsersController = require('../controllers/users')

router.post('/signup', UsersController.signup)
router.delete('/:userId', UsersController.delete_user)
router.post('/login', UsersController.login_user)

module.exports = router;