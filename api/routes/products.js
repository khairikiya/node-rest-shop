const express = require('express');
const router = express.Router();

const {
	update
} = require('../models/products');
const Product = require('../models/products');
const checkAuth = require('../middleware/check-auth');
const ProductsController = require('../controllers/products');

router.get('/', checkAuth, ProductsController.get_products);
router.post('/', checkAuth, ProductsController.add_product);
router.get('/:productId', checkAuth, ProductsController.get_product_by_id)
router.patch("/:productId", checkAuth, ProductsController.update_product_by_id)
router.delete('/:productId', checkAuth, ProductsController.delete_product)

module.exports = router;
