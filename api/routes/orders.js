const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const {
	request
} = require('../../app');

const Order = require('../models/orders');
const Product = require('../models/products');
const checkAuth = require('../middleware/check-auth')
const OrdersController = require('../controllers/orders');

router.get('/', checkAuth, OrdersController.orders_get_all)
router.post('/', checkAuth, OrdersController.post_orders)
router.get('/:orderId', checkAuth, OrdersController.get_orders_by_id)
router.delete('/:orderId', checkAuth, OrdersController.delete_orders)


module.exports = router;
