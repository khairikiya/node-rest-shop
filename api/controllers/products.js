const Product = require('../models/products');
const mongoose = require('mongoose');
exports.get_products = (req, res, next) => {
    Product.find().select('name price _id')
        .exec()
        .then(docs => {
            const response = {
                count: docs.length,
                products: docs.map(doc => {
                    return {
                        name: doc.name,
                        price: doc.price,
                        _id: doc._id,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/products/' + doc._id
                        }
                    }
                })
            }
            if (docs.length >= 0) {
                console.log(docs)
                res.status(200).json(response);
            } else {
                res.status(204).json({
                    message: "Products Empty"
                });
            }
        }).catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
}

exports.add_product = (req, res, next) => {


    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    });

    product.save().then(result => {
        console.log(result);
        res.status(201).json({
            message: "Created Product Success",
            createdProduct: {
                name: result.name,
                price: result.price,
                _id: result._id,
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/products/' + result._id
                }
            }
        });
    })
        .catch(err => {
            console.log(err)
            res.status(505).json({
                message: "Error add product",
                error: err
            });
        });
}

exports.get_product_by_id = (req, res, next) => {
    const id = req.params.productId;
    Product.findById(id)
        .select('name price _id')
        .exec()
        .then(doc => {
            console.log("From dabatase", doc);
            if (doc) {
                res.status(200).json({
                    product: doc,
                    request: {
                        type: 'GET',
                        url: 'http://localhost:3000/products/'
                    }
                });
            } else {
                res.status(404).json({
                    message: "No Valid Product"
                })
            }
        }).catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
}

exports.update_product_by_id = (req, res, next) => {
    const id = req.params.productId;
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Product.update({
        _id: id
    }, {
        $set: updateOps
    }).exec().then(result => {
        console.log(result);
        res.status(200).json({
            message: 'Product Updated',
            request: {
                type: 'GET',
                url: 'http://localhost:3000/products/' + id
            }
        });
    }).catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        })
    })
}

exports.delete_product = (req, res, next) => {
    const id = req.params.productId
    Product.deleteOne({
        _id: id
    }).exec()
        .then(result => {
            res.status(200).json({
                message: 'Product deleted',
                request: {
                    type: "POST",
                    url: "localhost/localhost:3000/products",
                    body: {
                        name: 'String',
                        price: 'Number'
                    }
                }
            });
        }).catch(err => {
            console.log(err);
            res.status(500).json({
                error: err
            })
        })
}